# README #

### Prerequisites ###

-gradle
-npm
-ng-cli
-internet connection (to connect to Mongo DB Atlas Cluster)


### How do I get set up? ###

*run "npm install" from springrest/frontend
*run "ng build" from springrest/frontend
*run "gradle clean build"from springrest/player

### How to run Application ###

*Application will run on embeded tomcat on http://localhost:8080

