import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-player-create',
  templateUrl: './player-create.component.html',
  styleUrls: ['./player-create.component.css']
})
export class PlayerCreateComponent implements OnInit {
  player = {};

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
  }
saveContact() {
    this.http.post('/contacts', this.player)
    .subscribe(res => {
    let id = res['id'];
    this.router.navigate(['/player-detail', id]);
    }, (err) => {
    console.log(err);
    }
    );
    }
}
