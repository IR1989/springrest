import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-player-edit',
  templateUrl: './player-edit.component.html',
  styleUrls: ['./player-edit.component.css']
})
export class PlayerEditComponent implements OnInit {
  player = {};

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getPlayer(this.route.snapshot.params['id']);
  }

  getPlayer(id) {
    this.http.get('api/player/'+id).subscribe(data => {
      this.player = data;
    });
  }

  updatePlayer(id, data) {
    debugger;
    this.http.put('api/player/'+id, data)
      .subscribe(res => {
          let id = res['id'];
          this.router.navigate(['/player-detail', id]);
        }, (err) => {
          console.log(err);
        }
      );
  }
}
