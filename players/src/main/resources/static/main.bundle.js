webpackJsonp(["main"],{

/***/ "./src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_gendir lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("./src/app/app.component.html"),
        styles: [__webpack_require__("./src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__player_player_component__ = __webpack_require__("./src/app/player/player.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__player_detail_player_detail_component__ = __webpack_require__("./src/app/player-detail/player-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__player_create_player_create_component__ = __webpack_require__("./src/app/player-create/player-create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__player_edit_player_edit_component__ = __webpack_require__("./src/app/player-edit/player-edit.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var appRoutes = [
    {
        path: 'player',
        component: __WEBPACK_IMPORTED_MODULE_6__player_player_component__["a" /* PlayerComponent */],
        data: { title: 'Players List' }
    },
    {
        path: 'player-detail/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__player_detail_player_detail_component__["a" /* PlayerDetailComponent */],
        data: { title: 'Player Details' }
    },
    { path: 'player-edit/:id',
        component: __WEBPACK_IMPORTED_MODULE_9__player_edit_player_edit_component__["a" /* PlayerEditComponent */],
        data: { title: 'Edit Player' }
    }, {
        path: 'player-create',
        component: __WEBPACK_IMPORTED_MODULE_8__player_create_player_create_component__["a" /* PlayerCreateComponent */],
        data: { title: 'Create Player' }
    },
    { path: '',
        redirectTo: '/player',
        pathMatch: 'full'
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__player_player_component__["a" /* PlayerComponent */],
            __WEBPACK_IMPORTED_MODULE_7__player_detail_player_detail_component__["a" /* PlayerDetailComponent */],
            __WEBPACK_IMPORTED_MODULE_8__player_create_player_create_component__["a" /* PlayerCreateComponent */],
            __WEBPACK_IMPORTED_MODULE_9__player_edit_player_edit_component__["a" /* PlayerEditComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */].forRoot(appRoutes, { enableTracing: true } // <-- debugging purposes only
            )
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/player-create/player-create.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/player-create/player-create.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n\r\n    <form (ngSubmit)=\"savePlayer()\" #playerForm=\"ngForm\">\r\n\r\n        <!-- first row -->\r\n        <div class=\"form-row\">\r\n            <div class=\"form-group col-md-4\">\r\n                <h2>Create Player</h2>\r\n                <label for=\"playername\">Name</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"playername\" [ngModel]=\"player?.name\" (ngModelChange)=\"player.name=$event\" placeholder=\"name\" name=\"playername\" required>\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <h2>Points</h2>\r\n                <label for=\"displayName\">Name</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"displayName\" [ngModel]=\"player?.units?.points?.displayName\" (ngModelChange)=\"player.units.points.displayName=$event\" placeholder=\"name\" name=\"displayName\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <h2>{{ player?.units?.unitGold?.name }}</h2>\r\n                <label for=\"goldAll\">All</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldAll\" [ngModel]=\"player?.units?.unitGold?.all\" (ngModelChange)=\"player.units.unitGold.all=$event\" placeholder=\"All\" name=\"goldAll\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <h2>Points</h2>\r\n                <label for=\"displayName\">Name</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"displayName\" [ngModel]=\"player?.units?.points?.displayName\" (ngModelChange)=\"player.units.points.displayName=$event\" placeholder=\"name\" name=\"displayName\">\r\n            </div>\r\n        </div>\r\n        <!-- second row-->\r\n        <div class=\"form-row\">\r\n            <!--div class=\"form-group col-md-4\">\r\n                        <img src=\"{{player?.image}}\" alt=\"\">\r\n                    </div> -->\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"all\">All</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <label for=\"goldOrder\">Order</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldOrder\" [ngModel]=\"player?.units?.unitGold?.order\" (ngModelChange)=\"player.units.unitGold.order=$event\" placeholder=\"order\" name=\"goldOrder\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n        </div>\r\n        <!-- third row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"year\">Year</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"year\" [ngModel]=\"player?.units?.points?.year\" (ngModelChange)=\"player.units.points.year=$event\" placeholder=\"year\" name=\"year\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <label for=\"goldType\">Type</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldType\" [ngModel]=\"player?.units?.unitGold?.type\" (ngModelChange)=\"player.units.unitGold.type=$event\" placeholder=\"type\" name=\"goldType\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"goldType\">Type</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldType\" [ngModel]=\"player?.units?.unitGold?.type\" (ngModelChange)=\"player.units.unitGold.type=$event\" placeholder=\"type\" name=\"goldType\">\r\n            </div>\r\n        </div>\r\n        <!-- forth row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"month\">Month</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"month\" [ngModel]=\"player?.units?.points?.month\" (ngModelChange)=\"player.units.points.month=$event\" placeholder=\"month\" name=\"month\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <label for=\"goldAbr\">Abbreviation</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldAbr\" [ngModel]=\"player?.units?.unitGold?.abbreviation\" (ngModelChange)=\"player.units.unitGold.abbreviation=$event\" placeholder=\"abbreviation\" name=\"goldAbr\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"goldType\">Type</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldType\" [ngModel]=\"player?.units?.unitGold?.type\" (ngModelChange)=\"player.units.unitGold.type=$event\" placeholder=\"type\" name=\"goldType\">\r\n            </div>\r\n        </div>\r\n        <!-- fifth row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"week\">Week</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"week\" [ngModel]=\"player?.units?.points?.week\" (ngModelChange)=\"player.units.points.week=$event\" placeholder=\"week\" name=\"week\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"day\">Day</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"day\" [ngModel]=\"player?.units?.points?.day\" (ngModelChange)=\"player.units.points.day=$event\" placeholder=\"day\" name=\"day\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n\r\n        </div>\r\n        <!-- seventh form-row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"abbrevation\">Abbrevation</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"abbrevation\" [ngModel]=\"player?.units?.points?.abbrevation\" (ngModelChange)=\"player.units.points.abbrevation=$event\" placeholder=\"abbrevation\" name=\"abbrevation\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n        </div>\r\n        <!-- button form-row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!playerForm.form.valid\">Save</button>\r\n        </div>\r\n    </form>\r\n</div>"

/***/ }),

/***/ "./src/app/player-create/player-create.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerCreateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerCreateComponent = (function () {
    function PlayerCreateComponent(http, router) {
        this.http = http;
        this.router = router;
        this.player = {};
    }
    PlayerCreateComponent.prototype.ngOnInit = function () {
    };
    PlayerCreateComponent.prototype.saveContact = function () {
        var _this = this;
        this.http.post('/contacts', this.player)
            .subscribe(function (res) {
            var id = res['id'];
            _this.router.navigate(['/player-detail', id]);
        }, function (err) {
            console.log(err);
        });
    };
    return PlayerCreateComponent;
}());
PlayerCreateComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player-create',
        template: __webpack_require__("./src/app/player-create/player-create.component.html"),
        styles: [__webpack_require__("./src/app/player-create/player-create.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object])
], PlayerCreateComponent);

var _a, _b;
//# sourceMappingURL=player-create.component.js.map

/***/ }),

/***/ "./src/app/player-detail/player-detail.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/player-detail/player-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-5  toppad  pull-right col-md-offset-3 \">\r\n\r\n            <br>\r\n            <p class=\" text-info\">May 05,2014,03:00 pm </p>\r\n        </div>\r\n        <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\">\r\n\r\n\r\n            <div class=\"panel panel-info\">\r\n                <div class=\"panel-heading\">\r\n                    <h3 class=\"panel-title\">{{ player?.displayName }}</h3>\r\n                </div>\r\n                <div class=\"panel-body\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-md-3 col-lg-3 \" align=\"center\"> <img alt=\"User Pic\" src=\"{{player.image}}\" class=\"img-circle img-responsive\"> </div>\r\n\r\n                        <div class=\" col-md-9 col-lg-9 \">\r\n                            <h2>Points</h2>\r\n                            <table class=\"table table-user-information\">\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td>Display name</td>\r\n                                        <td>{{ player?.units?.points?.displayName }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>All</td>\r\n                                        <td>{{ player?.units?.points?.all }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Year</td>\r\n                                        <td>{{ player?.units?.points?.year }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Month</td>\r\n                                        <td>{{ player?.units?.points?.month }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Week</td>\r\n                                        <td>{{ player?.units?.points?.week }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Day</td>\r\n                                        <td>{{ player?.units?.points?.day }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Abbreviation</td>\r\n                                        <td>{{ player?.units?.points?.abbrevation }}</td>\r\n                                    </tr>\r\n\r\n                                </tbody>\r\n                            </table>\r\n\r\n                        </div>\r\n                        <div *ngIf=\"player?.units?.unitGold != null\" class=\" col-md-9 col-lg-9 \">\r\n                            <h2>{{ player?.units?.unitGold?.name }}</h2>\r\n                            <table class=\"table table-user-information\">\r\n                                <tbody>\r\n                                    <tr>\r\n                                        <td>All</td>\r\n                                        <td>{{ player?.units?.unitGold?.displayName }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Order</td>\r\n                                        <td>{{ player?.units?.unitGold?.order }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Type</td>\r\n                                        <td>{{player?.units?.unitGold?.type }}</td>\r\n                                    </tr>\r\n                                    <tr>\r\n                                        <td>Abbreviation</td>\r\n                                        <td>{{ player?.units?.unitGold?.abbreviation }}</td>\r\n                                    </tr>\r\n\r\n\r\n                                </tbody>\r\n                            </table>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"panel-footer\">\r\n\r\n                    <span class=\"pull-right\">\r\n                          <a [routerLink]=\"['/player-edit', player.id]\"  data-original-title=\"Edit this player\" data-toggle=\"tooltip\" type=\"button\" class=\"btn btn-sm btn-warning\"><i class=\"glyphicon glyphicon-edit\"></i></a>\r\n                          <a (click)=\"deletePlayer(player.id)\" data-original-title=\"Remove this player\" data-toggle=\"tooltip\" type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"glyphicon glyphicon-remove\"></i></a>\r\n                      </span>\r\n                </div>\r\n\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "./src/app/player-detail/player-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerDetailComponent = (function () {
    function PlayerDetailComponent(router, route, http) {
        this.router = router;
        this.route = route;
        this.http = http;
        this.player = {};
    }
    PlayerDetailComponent.prototype.ngOnInit = function () {
        this.getPlayerDetail(this.route.snapshot.params['id']);
    };
    PlayerDetailComponent.prototype.getPlayerDetail = function (id) {
        var _this = this;
        var player = null;
        player = this.http.get('api/player/' + id);
        player.subscribe(function (data) {
            _this.player = data;
        });
    };
    PlayerDetailComponent.prototype.deletePlayer = function (id) {
        var _this = this;
        this.http.delete('api/player/' + id)
            .subscribe(function (res) {
            _this.router.navigate(['/player']);
        }, function (err) {
            console.log(err);
        });
    };
    return PlayerDetailComponent;
}());
PlayerDetailComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player-detail',
        template: __webpack_require__("./src/app/player-detail/player-detail.component.html"),
        styles: [__webpack_require__("./src/app/player-detail/player-detail.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _c || Object])
], PlayerDetailComponent);

var _a, _b, _c;
//# sourceMappingURL=player-detail.component.js.map

/***/ }),

/***/ "./src/app/player-edit/player-edit.component.css":
/***/ (function(module, exports) {

module.exports = "img {\r\n    height: 140px;\r\n    width: 140px;\r\n}"

/***/ }),

/***/ "./src/app/player-edit/player-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <form (ngSubmit)=\"updatePlayer(player.id,player)\" #playerForm=\"ngForm\">\r\n        <!-- first row -->\r\n        <div class=\"form-row\">\r\n            <div class=\"form-group col-md-4\">\r\n                <h2>Edit Player</h2>\r\n                <label for=\"playername\">Name</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"playername\" [ngModel]=\"player?.displayName\" (ngModelChange)=\"player.displayName=$event\" placeholder=\"name\" name=\"playername\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <h2>Points</h2>\r\n                <label for=\"displayName\">Name</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"displayName\" [ngModel]=\"player?.units?.points?.displayName\" (ngModelChange)=\"player.units.points.displayName=$event\" placeholder=\"name\" name=\"displayName\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <h2>{{ player?.units?.unitGold?.name }}</h2>\r\n                <label for=\"goldAll\">All</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldAll\" [ngModel]=\"player?.units?.unitGold?.all\" (ngModelChange)=\"player.units.unitGold.all=$event\" placeholder=\"All\" name=\"goldAll\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <h2>Points</h2>\r\n                <label for=\"displayName\">Name</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"displayName\" [ngModel]=\"player?.units?.points?.displayName\" (ngModelChange)=\"player.units.points.displayName=$event\" placeholder=\"name\" name=\"displayName\">\r\n            </div>\r\n        </div>\r\n        <!-- second row-->\r\n        <div class=\"form-row\">\r\n            <!--div class=\"form-group col-md-4\">\r\n                    <img src=\"{{player?.image}}\" alt=\"\">\r\n                </div> -->\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"all\">All</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <label for=\"goldOrder\">Order</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldOrder\" [ngModel]=\"player?.units?.unitGold?.order\" (ngModelChange)=\"player.units.unitGold.order=$event\" placeholder=\"order\" name=\"goldOrder\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n        </div>\r\n        <!-- third row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"year\">Year</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"year\" [ngModel]=\"player?.units?.points?.year\" (ngModelChange)=\"player.units.points.year=$event\" placeholder=\"year\" name=\"year\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <label for=\"goldType\">Type</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldType\" [ngModel]=\"player?.units?.unitGold?.type\" (ngModelChange)=\"player.units.unitGold.type=$event\" placeholder=\"type\" name=\"goldType\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"goldType\">Type</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldType\" [ngModel]=\"player?.units?.unitGold?.type\" (ngModelChange)=\"player.units.unitGold.type=$event\" placeholder=\"type\" name=\"goldType\">\r\n            </div>\r\n        </div>\r\n        <!-- forth row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"month\">Month</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"month\" [ngModel]=\"player?.units?.points?.month\" (ngModelChange)=\"player.units.points.month=$event\" placeholder=\"month\" name=\"month\">\r\n            </div>\r\n            <div *ngIf=\"player?.units?.unitGold != null \" class=\"form-group col-md-4\">\r\n                <label for=\"goldAbr\">Abbreviation</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldAbr\" [ngModel]=\"player?.units?.unitGold?.abbreviation\" (ngModelChange)=\"player.units.unitGold.abbreviation=$event\" placeholder=\"abbreviation\" name=\"goldAbr\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div *ngIf=\"player?.units?.unitGold == null \" class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"goldType\">Type</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"goldType\" [ngModel]=\"player?.units?.unitGold?.type\" (ngModelChange)=\"player.units.unitGold.type=$event\" placeholder=\"type\" name=\"goldType\">\r\n            </div>\r\n        </div>\r\n        <!-- fifth row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"week\">Week</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"week\" [ngModel]=\"player?.units?.points?.week\" (ngModelChange)=\"player.units.points.week=$event\" placeholder=\"week\" name=\"week\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"day\">Day</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"day\" [ngModel]=\"player?.units?.points?.day\" (ngModelChange)=\"player.units.points.day=$event\" placeholder=\"day\" name=\"day\">\r\n            </div>\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n\r\n        </div>\r\n        <!-- seventh form-row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\">\r\n                <label for=\"abbrevation\">Abbrevation</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"abbrevation\" [ngModel]=\"player?.units?.points?.abbrevation\" (ngModelChange)=\"player.units.points.abbrevation=$event\" placeholder=\"abbrevation\" name=\"abbrevation\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n        </div>\r\n        <!-- button form-row -->\r\n        <div class=\"form-row\">\r\n            <!-- hidden row -->\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <div class=\"form-group col-md-4\" style=\"visibility: hidden;\">\r\n                <label for=\"all\">Hidden</label>\r\n                <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" (ngModelChange)=\"player.units.points.all=$event\" placeholder=\"all\" name=\"all\">\r\n            </div>\r\n            <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!playerForm.form.valid\">Update</button>\r\n        </div>\r\n\r\n    </form>\r\n</div>"

/***/ }),

/***/ "./src/app/player-edit/player-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerEditComponent = (function () {
    function PlayerEditComponent(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.player = {};
    }
    PlayerEditComponent.prototype.ngOnInit = function () {
        this.getPlayer(this.route.snapshot.params['id']);
    };
    PlayerEditComponent.prototype.getPlayer = function (id) {
        var _this = this;
        this.http.get('api/player/' + id).subscribe(function (data) {
            _this.player = data;
        });
    };
    PlayerEditComponent.prototype.updatePlayer = function (id, data) {
        var _this = this;
        debugger;
        this.http.put('api/player/' + id, data)
            .subscribe(function (res) {
            var id = res['id'];
            _this.router.navigate(['/player-detail', id]);
        }, function (err) {
            console.log(err);
        });
    };
    return PlayerEditComponent;
}());
PlayerEditComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player-edit',
        template: __webpack_require__("./src/app/player-edit/player-edit.component.html"),
        styles: [__webpack_require__("./src/app/player-edit/player-edit.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
], PlayerEditComponent);

var _a, _b, _c;
//# sourceMappingURL=player-edit.component.js.map

/***/ }),

/***/ "./src/app/player/player.component.css":
/***/ (function(module, exports) {

module.exports = "img {\r\n    height: 140px;\r\n    width: 140px;\r\n}\r\n\r\ntd {\r\n    padding: 20px;\r\n}"

/***/ }),

/***/ "./src/app/player/player.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\r\n    <h1>Player List</h1>\r\n    <table class=\"table-striped\">\r\n        <thead>\r\n            <tr>\r\n                <th>Image</th>\r\n                <th>Display Name</th>\r\n                <th>Action</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let player of players\">\r\n                <td><img src=\"{{ player.image }}\" class=\"img-responsive img-rounded\" alt=\"\" /></td>\r\n                <td>{{ player.displayName }}</td>\r\n                <td><a [routerLink]=\"['/player-detail', player.id]\">Show Detail</a></td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n    <h1>Player List\r\n        <!-- <a [routerLink]=\"['/player-create']\" class=\"btn btn-default btn-lg\">\r\n            <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\r\n        </a> -->\r\n    </h1>\r\n</div>"

/***/ }),

/***/ "./src/app/player/player.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlayerComponent = (function () {
    function PlayerComponent(http) {
        this.http = http;
    }
    PlayerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('api/player').subscribe(function (data) {
            _this.players = data;
        });
    };
    return PlayerComponent;
}());
PlayerComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player',
        template: __webpack_require__("./src/app/player/player.component.html"),
        styles: [__webpack_require__("./src/app/player/player.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], PlayerComponent);

var _a;
//# sourceMappingURL=player.component.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map