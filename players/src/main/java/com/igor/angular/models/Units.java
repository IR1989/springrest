package com.igor.angular.models;

import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
//import com.fasterxml.jackson.annotation.*;

//import org.springframework.data.mongodb.core.mapping.DBRef;
//import org.springframework.data.annotation.Id;
////import org.springframework.data.mongodb.core.mapping.Document;

//@Document(collection = "playersDB")
@JsonInclude(JsonInclude.Include.NON_NULL)

public class Units {
    
    Points points;
    UnitGold unit_gold;
    
    public Units(){
    }
    
    public Units(Points points,UnitGold unit_gold){
            this.points=points;
            this.unit_gold=unit_gold;
        }
    public Points getPoints() {
        return this.points;
    }
    public void setPoints(Points points){
        this.points = points;
    }
    public UnitGold getUnitGold() {
        return this.unit_gold;
    }
    public void setUnitGold(UnitGold unit_gold) {
        this.unit_gold = unit_gold;
    }
    
}