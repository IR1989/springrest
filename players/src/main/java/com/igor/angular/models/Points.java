package com.igor.angular.models;

import com.fasterxml.jackson.annotation.JsonInclude;

// import org.omg.CosNaming.NamingContextExtPackage.StringNameHelper;
// import org.springframework.data.annotation.Id;
// import org.springframework.data.mongodb.core.mapping.Document;

//import com.fasterxml.jackson.annotation.*;

//@Document(collection = "playersDB")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Points {
    String display_name;
    String name;
    String abbreviation;
    String type;
    Double all;
    Double year;
    Double month;
    Double week;
    Double day;
    
    public Points(){
    }

    public Points(String display_name, String name,  String abbreviation, String type,  Double all, Double year, Double month, Double week, Double day){
    this.display_name=display_name;
    this.name=name;
    this. abbreviation=abbreviation;
    this.type=type;
    this.all=all;
    this.year=year;
    this.month=month;
    this.week=week;
    this.day=day;
    }
    public String getDisplayName() {
        return this.display_name;
    }
    public void setDisplayName(String display_name) {
        this.display_name = display_name;
    }
    
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAbbrevation() {
        return this.abbreviation;
    }
    public void setAbbrevation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }    
    public Double getAll() {
        return this.all;
    }
    public void setAll(Double all) {
        this.all = all;
    }
    public Double getYear() {
        return this.year;
    }
    public void setYear(Double year) {
        this.year = year;
    }
    public Double getMonth() {
        return this.month;
    }
    public void setMonth(Double month) {
        this.month = month;
    }
    public Double getWeek() {
        return this.week;
    }
    public void setWeek(Double week) {
        this.week = week;
    }
    public Double getDay() {
        return this.day;
    }
    public void setDay(Double day) {
        this.day = day;
    }
}