package com.igor.angular.models;

import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
//import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.*;

@Document(collection = "test2")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Player {
    @Id
    private String id;
    private String name;
    private String display_name;
    private String image;
    private Units units;

    public Player() {
    }

    public Player(String name, String display_name, String image,Units units) {
        this.name = name;
        this.display_name = display_name;
        this.image = image;
        this.units=units;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return display_name;
    }

    public void setDisplayName(String display_name) {
        this.display_name = display_name;
    }
    
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    public Units getUnits() {
        return this.units;
    }
    public void setUnits(Units units) {
        this.units = units;
    }
}