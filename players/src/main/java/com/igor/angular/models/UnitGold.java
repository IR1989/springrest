package com.igor.angular.models;
import com.fasterxml.jackson.annotation.*;
import org.springframework.data.annotation.Id;
// import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Document;

//import org.springframework.data.annotation.Id;

@Document
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UnitGold {
    
    @Id
    private String id;
    private String display_name;
    private String name;
    private String abbreviation;
    private String type;
    private Double order;
    private Double all;

    public UnitGold(){
    }

    public UnitGold(String display_name, String name, String abbreviation, String type, Double order, Double all){
    this.display_name=display_name;
    this.name=name;
    this.abbreviation=abbreviation;
    this.type=type;
    this.order=order;
    this.all=all;
    }

    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return this.display_name;
    }
    public void setDisplayName(String display_name) {
        this.display_name = display_name;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAbbreviation() {
        return this.abbreviation;
    }
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }
    public String getType() {
        return this.type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Double getOrder() {
        return this.order;
    }
    public void setOrder(Double order) {
        this.order = order;
    }
    public Double getAll() {
        return this.all;
    }
    public void setAll(Double all) {
        this.all = all;
    }
    
}

