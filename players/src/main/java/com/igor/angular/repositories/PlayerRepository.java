package com.igor.angular.repositories;

import com.igor.angular.models.Player;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.mongodb.repository.MongoRepository;

public interface PlayerRepository extends CrudRepository<Player, String> {
    @Override
    Player findOne(String id);

    @Override
    void delete(Player deleted);
}