// package com.igor.angular.controllers;

// import com.igor.angular.models.Points;
// import com.igor.angular.repositories.PointsRepository;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RestController;

// @RestController
// public class PointsController {

//     @Autowired
//     PointsRepository pointsRepository;

//     @RequestMapping(method=RequestMethod.GET, value="/points")
//     public Iterable<Points> points() {
//         return pointsRepository.findAll();
//     }

//     @RequestMapping(method=RequestMethod.POST, value="/points")
//     public Points save(@RequestBody Points points) {
//         pointsRepository.save(points);

//         return points;
//     }

//     @RequestMapping(method=RequestMethod.GET, value="/points/{id}")
//     public Points show(@PathVariable String id) {
//         return pointsRepository.findOne(id);
//     }

//     @RequestMapping(method=RequestMethod.PUT, value="/points/{id}")
//     public Points update(@PathVariable String id, @RequestBody Points points) {
//         Points p = pointsRepository.findOne(id);
//         if(points.getUnits() != null)
//             p.setUnits(points.getUnits());
//         pointsRepository.save(p);
//         return points;
//     }

//     @RequestMapping(method=RequestMethod.DELETE, value="/points/{id}")
//     public String delete(@PathVariable String id) {
//         Points points  = pointsRepository.findOne(id);
//         pointsRepository.delete(points);

//         return "";
//     }
// }