// package com.igor.angular.controllers;

// import com.igor.angular.models.Units;
// import com.igor.angular.repositories.UnitsRepository;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.web.bind.annotation.PathVariable;
// import org.springframework.web.bind.annotation.RequestBody;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.RestController;

// @RestController
// public class UnitsController {

//     @Autowired
//     UnitsRepository unitsRepository;

//     @RequestMapping(method=RequestMethod.GET, value="/units")
//     public Iterable<Units> units() {
//         return unitsRepository.findAll();
//     }

//     @RequestMapping(method=RequestMethod.POST, value="/units")
//     public Units save(@RequestBody Units units) {
//         unitsRepository.save(units);

//         return units;
//     }

//     @RequestMapping(method=RequestMethod.GET, value="/units/{id}")
//     public Units show(@PathVariable String id) {
//         return unitsRepository.findOne(id);
//     }

//     @RequestMapping(method=RequestMethod.PUT, value="/units/{id}")
//     public Units update(@PathVariable String id, @RequestBody Units units) {
//         Units u = unitsRepository.findOne(id);
//         if(units.getDisplayName() != null)
//             u.setDisplayName(units.getDisplayName());
//         if(units.getName() != null)
//             u.setName(units.getName());
//         if(units.getAbbrevation() != null)
//             u.setAbbrevation(units.getAbbrevation());
//         if(units.getType() != null)
//             u.setType(units.getType());
//         if(units.getAll() != null)
//             u.setAll(units.getAll());
//         if(units.getYear() != null)
//             u.setYear(units.getYear());
//         if(units.getMonth() != null)
//             u.setMonth(units.getMonth());
//         if(units.getWeek() != null)
//             u.setWeek(units.getWeek());
//         if(units.getDay() != null)
//             u.setDay(units.getDay());
        
//         unitsRepository.save(u);
//         return units;
//     }

//     @RequestMapping(method=RequestMethod.DELETE, value="/units/{id}")
//     public String delete(@PathVariable String id) {
//         Units units = unitsRepository.findOne(id);
//         unitsRepository.delete(units);

//         return "";
//     }
// }