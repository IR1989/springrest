package com.igor.angular.controllers;

import com.igor.angular.models.Player;
import com.igor.angular.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PlayerController {

    @Autowired
    PlayerRepository playerRepository;

    @RequestMapping(method=RequestMethod.GET, value="/player")
    public Iterable<Player> player() {
        return playerRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/player")
    public Player save(@RequestBody Player player) {
        playerRepository.save(player);

        return player;
    }

    @RequestMapping(method=RequestMethod.GET, value="/player/{id}")
    public Player show(@PathVariable String id) {
        return playerRepository.findOne(id);
    }

    @RequestMapping(method=RequestMethod.PUT, value="/player/{id}")
    public Player update(@PathVariable String id, @RequestBody Player player) {
        Player p = playerRepository.findOne(id);
        if(player.getId() != null)
            p.setId(player.getId());
        if(player.getName() != null)
            p.setName(player.getName());
        if(player.getDisplayName() != null)
            p.setDisplayName(player.getDisplayName());
        if(player.getImage() != null)
            p.setImage(player.getImage());
        if(player.getUnits() != null)
            p.setUnits(player.getUnits());
        playerRepository.save(p);
        return player;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/player/{id}")
    public String delete(@PathVariable String id) {
        Player player = playerRepository.findOne(id);
        playerRepository.delete(player);

        return "";
    }
}