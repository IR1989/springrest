package com.igor.angular;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.fasterxml.jackson.databind.*;
import com.igor.angular.models.Player;
import com.fasterxml.jackson.core.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class PlayersApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void givenNullsIgnoredOnClass_whenWritingObjectWithNullField_thenIgnored()
	  throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		Player dtoObject = new Player();
	 
		String dtoAsString = mapper.writeValueAsString(dtoObject);
	 
		//assertThat(dtoAsString, containsString("intValue"));
		assertThat(dtoAsString, not(containsString("stringValue")));
	}
}
