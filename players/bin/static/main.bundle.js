webpackJsonp(["main"],{

/***/ "./src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_gendir lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("./src/app/app.component.html"),
        styles: [__webpack_require__("./src/app/app.component.css")]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "./src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("./node_modules/@angular/platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("./node_modules/@angular/forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__("./src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__player_player_component__ = __webpack_require__("./src/app/player/player.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__player_detail_player_detail_component__ = __webpack_require__("./src/app/player-detail/player-detail.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__player_create_player_create_component__ = __webpack_require__("./src/app/player-create/player-create.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__player_edit_player_edit_component__ = __webpack_require__("./src/app/player-edit/player-edit.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










var appRoutes = [
    {
        path: 'player',
        component: __WEBPACK_IMPORTED_MODULE_6__player_player_component__["a" /* PlayerComponent */],
        data: { title: 'Players List' }
    },
    {
        path: 'player-detail/:id',
        component: __WEBPACK_IMPORTED_MODULE_7__player_detail_player_detail_component__["a" /* PlayerDetailComponent */],
        data: { title: 'Player Details' }
    },
    { path: 'player-edit/:id',
        component: __WEBPACK_IMPORTED_MODULE_9__player_edit_player_edit_component__["a" /* PlayerEditComponent */],
        data: { title: 'Edit Player' }
    }, {
        path: 'player-create',
        component: __WEBPACK_IMPORTED_MODULE_8__player_create_player_create_component__["a" /* PlayerCreateComponent */],
        data: { title: 'Create Player' }
    },
    { path: '',
        redirectTo: '/player',
        pathMatch: 'full'
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["M" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__player_player_component__["a" /* PlayerComponent */],
            __WEBPACK_IMPORTED_MODULE_7__player_detail_player_detail_component__["a" /* PlayerDetailComponent */],
            __WEBPACK_IMPORTED_MODULE_8__player_create_player_create_component__["a" /* PlayerCreateComponent */],
            __WEBPACK_IMPORTED_MODULE_9__player_edit_player_edit_component__["a" /* PlayerEditComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["b" /* HttpClientModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* RouterModule */].forRoot(appRoutes, { enableTracing: true } // <-- debugging purposes only
            )
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "./src/app/player-create/player-create.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/player-create/player-create.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h1>Add New Player</h1>\n    <div class=\"row\">\n        <div class=\"col-md-6\">\n            <form (ngSubmit)=\"savePlayer()\" #playerForm=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Name</label>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"player.name\" name=\"name\" required>\n                </div>\n                <!-- <div class=\"form-group\">\n          <label for=\"name\">Address</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"contact.address\" name=\"address\" required>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"name\">City</label>\n          <input type=\"text\" class=\"form-control\" [(ngModel)]=\"contact.city\" name=\"city\" required>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"name\">Phone</label>\n          <input type=\"phone\" class=\"form-control\" [(ngModel)]=\"contact.phone\" name=\"phone\" required>\n        </div>\n        <div class=\"form-group\">\n          <label for=\"name\">Email</label>\n          <input type=\"email\" class=\"form-control\" [(ngModel)]=\"contact.email\" name=\"email\" required>\n        </div> -->\n                <div class=\"form-group\">\n                    <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!playerForm.form.valid\">Save</button>\n                </div>\n            </form>\n        </div>\n    </div>\n</div>]"

/***/ }),

/***/ "./src/app/player-create/player-create.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerCreateComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerCreateComponent = (function () {
    function PlayerCreateComponent(http, router) {
        this.http = http;
        this.router = router;
        this.player = {};
    }
    PlayerCreateComponent.prototype.ngOnInit = function () {
    };
    PlayerCreateComponent.prototype.saveContact = function () {
        var _this = this;
        this.http.post('/player', this.player)
            .subscribe(function (res) {
            _this.router.navigate(['/player-detail', res]);
        }, function (err) {
            console.log(err);
        });
    };
    return PlayerCreateComponent;
}());
PlayerCreateComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player-create',
        template: __webpack_require__("./src/app/player-create/player-create.component.html"),
        styles: [__webpack_require__("./src/app/player-create/player-create.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object])
], PlayerCreateComponent);

var _a, _b;
//# sourceMappingURL=player-create.component.js.map

/***/ }),

/***/ "./src/app/player-detail/player-detail.component.css":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/player-detail/player-detail.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <div class=\"row\">\n        <div class=\"col-md-5  toppad  pull-right col-md-offset-3 \">\n\n            <br>\n            <p class=\" text-info\">May 05,2014,03:00 pm </p>\n        </div>\n        <div class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad\">\n\n\n            <div class=\"panel panel-info\">\n                <div class=\"panel-heading\">\n                    <h3 class=\"panel-title\">{{ player?.displayName }}</h3>\n                </div>\n                <div class=\"panel-body\">\n                    <div class=\"row\">\n                        <div class=\"col-md-3 col-lg-3 \" align=\"center\"> <img alt=\"User Pic\" src=\"{{player.image}}\" class=\"img-circle img-responsive\"> </div>\n\n                        <div class=\" col-md-9 col-lg-9 \">\n                            <h2>Points</h2>\n                            <table class=\"table table-user-information\">\n                                <tbody>\n                                    <tr>\n                                        <td>Display name</td>\n                                        <td>{{ player?.units?.points?.displayName }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>All</td>\n                                        <td>{{ player?.units?.points?.all }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Year</td>\n                                        <td>{{ player?.units?.points?.year }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Month</td>\n                                        <td>{{ player?.units?.points?.month }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Week</td>\n                                        <td>{{ player?.units?.points?.week }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Day</td>\n                                        <td>{{ player?.units?.points?.day }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Abbreviation</td>\n                                        <td>{{ player?.units?.points?.abbrevation }}</td>\n                                    </tr>\n\n                                </tbody>\n                            </table>\n\n                        </div>\n                        <div *ngIf=\"player?.units?.unitGold != null\" class=\" col-md-9 col-lg-9 \">\n                            <h2>{{ player?.units?.unitGold?.name }}</h2>\n                            <table class=\"table table-user-information\">\n                                <tbody>\n                                    <tr>\n                                        <td>All</td>\n                                        <td>{{ player?.units?.unitGold?.displayName }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Order</td>\n                                        <td>{{ player?.units?.unitGold?.order }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Type</td>\n                                        <td>{{player?.units?.unitGold?.type }}</td>\n                                    </tr>\n                                    <tr>\n                                        <td>Abbreviation</td>\n                                        <td>{{ player?.units?.unitGold?.abbreviation }}</td>\n                                    </tr>\n\n\n                                </tbody>\n                            </table>\n\n                        </div>\n                    </div>\n                </div>\n                <div class=\"panel-footer\">\n\n                    <span class=\"pull-right\">\n                          <a [routerLink]=\"['/player-edit', player.id]\"  data-original-title=\"Edit this player\" data-toggle=\"tooltip\" type=\"button\" class=\"btn btn-sm btn-warning\"><i class=\"glyphicon glyphicon-edit\"></i></a>\n                          <a (click)=\"deletePlayer(player.id)\" data-original-title=\"Remove this player\" data-toggle=\"tooltip\" type=\"button\" class=\"btn btn-sm btn-danger\"><i class=\"glyphicon glyphicon-remove\"></i></a>\n                      </span>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./src/app/player-detail/player-detail.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerDetailComponent = (function () {
    function PlayerDetailComponent(router, route, http) {
        this.router = router;
        this.route = route;
        this.http = http;
        this.player = {};
    }
    PlayerDetailComponent.prototype.ngOnInit = function () {
        this.getPlayerDetail(this.route.snapshot.params['id']);
    };
    PlayerDetailComponent.prototype.getPlayerDetail = function (id) {
        var _this = this;
        var player = null;
        player = this.http.get('api/player/' + id);
        player.subscribe(function (data) {
            _this.player = data;
        });
    };
    PlayerDetailComponent.prototype.deletePlayer = function (id) {
        var _this = this;
        this.http.delete('api/player/' + id)
            .subscribe(function (res) {
            _this.router.navigate(['/player']);
        }, function (err) {
            console.log(err);
        });
    };
    return PlayerDetailComponent;
}());
PlayerDetailComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player-detail',
        template: __webpack_require__("./src/app/player-detail/player-detail.component.html"),
        styles: [__webpack_require__("./src/app/player-detail/player-detail.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _c || Object])
], PlayerDetailComponent);

var _a, _b, _c;
//# sourceMappingURL=player-detail.component.js.map

/***/ }),

/***/ "./src/app/player-edit/player-edit.component.css":
/***/ (function(module, exports) {

module.exports = "img {\r\n    height: 140px;\r\n    width: 140px;\r\n}"

/***/ }),

/***/ "./src/app/player-edit/player-edit.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n\n    <!-- <div class=\"row\">\n        <div class=\"col-md-6\">\n            <form (ngSubmit)=\"updatePlayer(player.id)\" #playerForm=\"ngForm\">\n                <div class=\"form-group\">\n                    <label for=\"name\">Name</label>\n                    <input type=\"text\" class=\"form-control\" [(ngModel)]=\"player.name\" name=\"name\" required>\n                </div>\n               \n                <div class=\"form-group\">\n                    <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!playerForm.form.valid\">Update</button>\n                </div>\n            </form>\n        </div>\n    </div> -->\n    <form (ngSubmit)=\"updatePlayer(player.id)\" #playerForm=\"ngForm\">\n        <div class=\"form-row\">\n\n\n            <div class=\"col-xs-6 form-group\">\n                <h2>Edit Player</h2>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"playername\">Name</label>\n                    <input type=\"text\" class=\"form-control\" id=\"playername\" [ngModel]=\"player?.name\" placeholder=\"name\" name=\"playername\">\n                </div>\n                <div class=\"form-group row-md-6\">\n                    <img src=\"{{player?.image}}\" alt=\"\">\n                </div>\n            </div>\n\n\n            <div class=\"col-xs-6 form-group\">\n                <h2>Points</h2>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"displayName\">Name</label>\n                    <input type=\"text\" class=\"form-control\" id=\"displayName\" [ngModel]=\"player?.units?.points?.displayName\" placeholder=\"name\" name=\"displayName\">\n                </div>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"all\">All</label>\n                    <input type=\"text\" class=\"form-control\" id=\"all\" [ngModel]=\"player?.units?.points?.all\" placeholder=\"all\" name=\"all\">\n                </div>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"year\">Year</label>\n                    <input type=\"text\" class=\"form-control\" id=\"year\" [ngModel]=\"player?.units?.points?.year\" placeholder=\"year\" name=\"year\">\n                </div>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"month\">Month</label>\n                    <input type=\"text\" class=\"form-control\" id=\"month\" [ngModel]=\"player?.units?.points?.month\" placeholder=\"month\" name=\"month\">\n                </div>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"week\">Week</label>\n                    <input type=\"text\" class=\"form-control\" id=\"week\" [ngModel]=\"player?.units?.points?.week\" placeholder=\"week\" name=\"week\">\n                </div>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"day\">Day</label>\n                    <input type=\"text\" class=\"form-control\" id=\"day\" [ngModel]=\"player?.units?.points?.day\" placeholder=\"day\" name=\"day\">\n                </div>\n                <div class=\"form-group col-md-6\">\n                    <label for=\"abbrevation\">Abbrevation</label>\n                    <input type=\"text\" class=\"form-control\" id=\"abbrevation\" [ngModel]=\"player?.units?.points?.abbrevation\" placeholder=\"abbrevation\" name=\"abbrevation\">\n                </div>\n            </div>\n            <div class=\"form-group\">\n                <button type=\"submit\" class=\"btn btn-success\" [disabled]=\"!playerForm.form.valid\">Update</button>\n            </div>\n        </div>\n    </form>\n</div>"

/***/ }),

/***/ "./src/app/player-edit/player-edit.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerEditComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__("./node_modules/@angular/router/@angular/router.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PlayerEditComponent = (function () {
    function PlayerEditComponent(http, router, route) {
        this.http = http;
        this.router = router;
        this.route = route;
        this.player = {};
    }
    PlayerEditComponent.prototype.ngOnInit = function () {
        this.getPlayer(this.route.snapshot.params['id']);
    };
    PlayerEditComponent.prototype.getPlayer = function (id) {
        var _this = this;
        this.http.get('api/player/' + id).subscribe(function (data) {
            _this.player = data;
        });
    };
    PlayerEditComponent.prototype.updatePlayer = function (id, data) {
        var _this = this;
        this.http.put('api/player/' + id, data)
            .subscribe(function (res) {
            var id = res['id'];
            _this.router.navigate(['/player-detail', id]);
        }, function (err) {
            console.log(err);
        });
    };
    return PlayerEditComponent;
}());
PlayerEditComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player-edit',
        template: __webpack_require__("./src/app/player-edit/player-edit.component.html"),
        styles: [__webpack_require__("./src/app/player-edit/player-edit.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["b" /* Router */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* ActivatedRoute */]) === "function" && _c || Object])
], PlayerEditComponent);

var _a, _b, _c;
//# sourceMappingURL=player-edit.component.js.map

/***/ }),

/***/ "./src/app/player/player.component.css":
/***/ (function(module, exports) {

module.exports = "img {\r\n    height: 140px;\r\n    width: 140px;\r\n}\r\n\r\ntd {\r\n    padding: 20px;\r\n}"

/***/ }),

/***/ "./src/app/player/player.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n    <h1>Player List</h1>\n    <table class=\"table-striped\">\n        <thead>\n            <tr>\n                <th>Image</th>\n                <th>Display Name</th>\n                <th>Action</th>\n            </tr>\n        </thead>\n        <tbody>\n            <tr *ngFor=\"let player of players\">\n                <td><img src=\"{{ player.image }}\" class=\"img-responsive img-rounded\" alt=\"\" /></td>\n                <td>{{ player.displayName }}</td>\n                <td><a [routerLink]=\"['/player-detail', player.id]\">Show Detail</a></td>\n            </tr>\n        </tbody>\n    </table>\n    <h1>Player List\n        <a [routerLink]=\"['/player-create']\" class=\"btn btn-default btn-lg\">\n            <span class=\"glyphicon glyphicon-plus\" aria-hidden=\"true\"></span>\n        </a>\n    </h1>\n    <!-- <div class=\"container-fluid\">\n        <div class=\"row\">\n\n            <div class=\"col-md-4\">Image</div>\n            <div class=\"col-md-4\">Display Name</div>\n            <div class=\"col-md-4\">Action</div>\n        </div>\n        <div class=\"row\" *ngFor=\"let player of players\">\n            <div class=\"col-md-4\"><img class=\"img-responsive img-circle\" src=\"{{ player.image }}\" alt=\"\" /></div>\n            <div class=\"col-md-4\">{{ player.displayName }}</div>\n            <div class=\"col-md-4\"><a [routerLink]=\"['/player-detail', player.id]\">Show Detail</a></div>\n        </div>\n\n    </div> -->"

/***/ }),

/***/ "./src/app/player/player.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PlayerComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__("./node_modules/@angular/common/@angular/common/http.es5.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlayerComponent = (function () {
    function PlayerComponent(http) {
        this.http = http;
    }
    PlayerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('api/player').subscribe(function (data) {
            _this.players = data;
        });
    };
    return PlayerComponent;
}());
PlayerComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["o" /* Component */])({
        selector: 'app-player',
        template: __webpack_require__("./src/app/player/player.component.html"),
        styles: [__webpack_require__("./src/app/player/player.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
], PlayerComponent);

var _a;
//# sourceMappingURL=player.component.js.map

/***/ }),

/***/ "./src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "./src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("./node_modules/@angular/core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("./node_modules/@angular/platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("./src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("./src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_23" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map